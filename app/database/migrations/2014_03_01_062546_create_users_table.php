<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	public function up()
	{
		Schema::table('users', function(Blueprint $table)
		{
			$table->string('id', 8)->primary();
            $table->string('fullname', 128);
            $table->string('password', 64);
            $table->string('email', 128);
            $table->timestamps();
		});
	}

	public function down()
	{
		Schema::drop('users');
	}

}