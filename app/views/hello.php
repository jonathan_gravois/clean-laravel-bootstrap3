<!doctype html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>WynneWade Enterprise</title>
	<style>
		@import url(//fonts.googleapis.com/css?family=Lato:700);

		body {
			margin:0;
			font-family:'Lato', sans-serif;
			text-align:center;
			background-color: #000;
			color: #999;
		}

		.welcome {
			width: 300px;
			height: 200px;
			position: absolute;
			left: 50%;
			top: 50%;
			margin-left: -150px;
			margin-top: -250px;
		}

		a, a:visited {
			text-decoration:none;
		}

		h1 {
			font-size: 32px;
			margin: 16px 0 0 0;
		}
	</style>
</head>
<body>
	<div class="welcome">
		<h1>You have arrived.</h1>
		<a href="http://laravel.com" title="Laravel PHP Framework">
			<img src="images/card.png" alt="Logo">
		</a>
	</div>
</body>
</html>
